# WT_Project

## Name
Campus in webVR

## Description
This project is a simple webVR representation of Odisee Ghent with some very basic info displayed.

## Installation
First of all: download or import the project files. Then do "npm install" to install all needed packages. After this you can start making changes if you want, but to see the result you'll need to do "npm start" to locally host/display the project with vite. The webVR part of this application can only be seen with compatible devices such as smartphones (and not your current computer). So to test/display this you'll need to do "npm run build" to build the project and upload everything in the "dist" folder (not the folder itself, only its contents) that is generated to a hosting site so you can open this with a compatible device such as a smartphone. The assets in the folder "assets" will also be needed on the hosting site, so add all the content to the folder "assets" currently on your hosting.

## Support
If you have problems with this project you can reach me at aaron.odent@student.odisee.be

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## License
This is a open source project with a MIT license.

## Project status
The main functionality is finished but improvements can surely be made, especially with the 3D file that's imported into three.js
