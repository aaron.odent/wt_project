import {OrbitControls} from "three/addons/controls/OrbitControls.js";
import {VRButton} from "three/addons/webxr/VRButton.js";
import {arrowMarker, map} from "./minimap";
import {Point} from "ol/geom";
import {fromLonLat, transform} from "ol/proj";
import {Euler, PerspectiveCamera, Ray, Scene, Vector3, WebGLRenderer} from "three";
import {StereoEffect} from "three/addons/effects/StereoEffect.js";
import {infopanelsGroup, infoplaceholdersGroup} from "./scene/infopanels.js";
import {User} from "./user";
import {ambientLight, pointLight} from "./scene/lights.js";
import {campus} from "./scene/campus.js";

// renderer
const renderer = new WebGLRenderer();
renderer.setPixelRatio(window.devicePixelRatio);
document.getElementById("canvas").appendChild(renderer.domElement);

// effect
const effect = new StereoEffect(renderer);
effect.setSize(window.innerWidth, window.innerHeight);

// camera
const cameraStartPosition = new Vector3(0, 100, 50);
const camera = new PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000
);
camera.position.copy(cameraStartPosition);

// user
const user = new User(camera);

// set scene
const scene = new Scene();
scene.add(await campus, infopanelsGroup, infoplaceholdersGroup);
scene.add(user)
scene.add(pointLight, ambientLight)

// controls
const controls = new OrbitControls(camera, renderer.domElement);
controls.keys = {
    UP: "ArrowUp",
    BOTTOM: "ArrowDown",
    LEFT: "ArrowLeft",
    RIGHT: "ArrowRight",
};
controls.listenToKeyEvents(window);
controls.keyPanSpeed = 15;

// region enable webVR
const VRbutton = VRButton.createButton(renderer);
document.body.appendChild(VRbutton);
renderer.xr.enabled = true;

// region animationloop
renderer.setAnimationLoop(function () {
    if (renderer.xr.isPresenting && user.movementEnabled) {
        user.move();
    }

    if (renderer.xr.isPresenting) {
        user.directCrossHair();
        user.positionBeenReset = false;
        user.setPosition();
        checkLookDirection(user.getLookDirection());
    } else {
        user.userPositionBeenSet = false;
        user.resetPosition();
    }

    moveArrowMarker();

    renderer.render(scene, camera);
    // effect.render(scene, camera);
});

function checkLookDirection(lookAtVector) {

    infoplaceholdersGroup.children.forEach((element, index) => {
        const directionToObject = new Vector3(
            element.position.x - user.position.x,
            element.position.y - camera.position.y,
            element.position.z - user.position.z
        );
        if (lookAtVector.angleTo(directionToObject) < (Math.PI / 180) * 5) {
            element.visible = false;
            infopanelsGroup.children[index].visible = true;
        } else {
            element.visible = true;
            infopanelsGroup.children[index].visible = false;
        }
    });
}

function moveArrowMarker() {
    const angle = (-35 * Math.PI) / 180;
    const angle2 = (-55 * Math.PI) / 180;
    const x = camera.position.x / 85000;
    const z = camera.position.z / 85000;

    const long = 3.7090911 - Math.cos(angle) * x - Math.sin(angle2) * z;
    const lat = 51.0601373 - Math.sin(angle) * x + Math.cos(angle2) * z;

    arrowMarker.setGeometry(new Point(fromLonLat([long, lat])));
    map.getView().setCenter(transform([long, lat], "EPSG:4326", "EPSG:3857"));

    const euler = new Euler();
    euler.setFromQuaternion(camera.quaternion);
    const relativeAngle = euler.z;
    const mapAngle = relativeAngle + (132.5 * Math.PI) / 180;
    map.getView().setRotation(mapAngle);
}

function printToConsole(value) {
    document.body.addEventListener("click", () => {
        console.log("value : " + value + "\n");
    });
}
