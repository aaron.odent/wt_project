import {Sprite, SpriteMaterial, TextureLoader} from "three";

const crossHairImage = new TextureLoader().load("assets/crosshair.png");
const crossHairMaterial = new SpriteMaterial({map: crossHairImage});
const crossHair = new Sprite(crossHairMaterial);

crossHair.scale.set(0.5, 0.5, 0.5);

export {crossHair} ;