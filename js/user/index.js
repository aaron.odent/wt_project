import {CircleGeometry, Clock, Group, Vector3} from "three";
import {button, buttonIndicator, standStillImage, walkImage} from "./button.js";
import {crossHair} from "./crossHair.js";

class User extends Group {

    buttonPressTime = 1;
    userStartPosition = new Vector3(0, 0, 0);
    movementEnabled = true;
    positionBeenReset = false;
    userPositionBeenSet = false;

    watch = new Clock();

    constructor(camera) {
        super();

        this.camera = camera;
        this.cameraStartPosition = camera.position;
        this.add(camera);

        this.position.copy(this.userStartPosition);

        this.add(button, buttonIndicator, crossHair);
    }

    move() {
        const vector = new Vector3(0, 0, -0.03);
        const direction = vector.applyQuaternion(this.camera.quaternion);
        direction.y = 0; //no up or down
        this.position.add(direction);
    }

    setPosition() {
        if (!this.userPositionBeenSet) {
            this.position.copy(this.userStartPosition);
            this.userPositionBeenSet = true;
        }
    }

    resetPosition() {
        if (!this.positionBeenReset) {
            this.position.copy(this.userStartPosition);
            this.camera.position.copy(this.cameraStartPosition);
            this.positionBeenReset = true;
        }
    }

    directCrossHair() {
        const vector = new Vector3(0, 0, -1);
        const direction = vector.applyQuaternion(this.camera.quaternion);
        direction.y += 1.62;
        crossHair.position.copy(direction);

        button.rotation.z = this.camera.rotation.z;
        buttonIndicator.rotation.z = this.camera.rotation.z;
    }

    getLookDirection() {
        const lookAtVector = new Vector3(0, 0, -1);
        const lookDownVector = new Vector3(0, -1, 0);

        lookAtVector.applyQuaternion(this.camera.quaternion);

        if (lookAtVector.angleTo(lookDownVector) < Math.PI / 8) {
            this.fillIndicator();
        } else {
            this.watch.stop();
            this.watch.elapsedTime = 0;
            this.updateIndicator();
        }

        return lookAtVector;
    }

    fillIndicator() {
        if (!this.watch.running) {
            this.watch.start();
        }
        else if (this.watch.getElapsedTime() >= this.buttonPressTime) {
            this.watch.stop();
            this.watch.elapsedTime = 0;
            this.movementEnabled = !this.movementEnabled;
            button.material.map = this.movementEnabled ? standStillImage : walkImage;
        }
        this.updateIndicator();
    }

    updateIndicator() {
        buttonIndicator.geometry.dispose();
        const fillAngle = (this.watch.getElapsedTime() / this.buttonPressTime) * Math.PI * 2;
        buttonIndicator.geometry = new CircleGeometry(
            0.55,
            32,
            Math.PI / 2 - fillAngle,
            fillAngle
        );
    }
}


export {User}