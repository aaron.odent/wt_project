import {CircleGeometry, Mesh, MeshBasicMaterial, TextureLoader} from "three";

const textureLoader = new TextureLoader();
const [walkImage, standStillImage] = await Promise.all([
    textureLoader.loadAsync("assets/walking.png"),
    textureLoader.loadAsync("assets/standStill.png")
])
const buttonGeometry = new CircleGeometry(0.5, 32);
const buttonMaterial = new MeshBasicMaterial({
    map: walkImage,
    color: 0xffffff,
});
const button = new Mesh(buttonGeometry, buttonMaterial);
button.position.y = 0.02;
button.rotation.x = -Math.PI / 2;

const buttonIndicatorGeometry = new CircleGeometry(
    0.55,
    32,
    0,
    Math.PI * 2
);
const buttonIndicatorMaterial = new MeshBasicMaterial({
    color: 0x33c2ff,
});
const buttonIndicator = new Mesh(
    buttonIndicatorGeometry,
    buttonIndicatorMaterial
);
buttonIndicator.position.y = 0.01;
buttonIndicator.rotation.x = -Math.PI / 2;

export {button, buttonIndicator, standStillImage, walkImage}