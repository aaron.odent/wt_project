import {GLTFLoader} from "three/addons/loaders/GLTFLoader.js";

const campus = new GLTFLoader().loadAsync("assets/campus.glb").then(gltf => {
    gltf.scene.position.set(-120, 0, 50);
    return gltf.scene;
})

export {campus}