import {Group, Sprite, SpriteMaterial, TextureLoader, Vector3} from "three";

class InfoPanel extends Sprite {
    constructor(position, imagePath) {
        const map = new TextureLoader().load(imagePath);
        const spriteMaterial = new SpriteMaterial({map});

        super(spriteMaterial)

        this.scale.set(20, 10, 20);
        this.position.copy(position);
        this.visible = false;
    }
}

class InfoPanelPlaceholder extends Sprite {
    constructor(position) {
        const map = new TextureLoader().load("assets/info.png");
        const spriteMaterial = new SpriteMaterial({map});

        super(spriteMaterial);

        this.scale.set(5, 5, 5);
        this.position.copy(position);
    }
}

const locationA = new Vector3(-10, 10, -60);
const locationB = new Vector3(100, 10, -30);
const locationC = new Vector3(80, 5, -5);
const locationD = new Vector3(60, 10, 20);
const locationE = new Vector3(10, 10, 0);
const locationF = new Vector3(-80, 10, -80);
const locationG = new Vector3(-20, 10, -110);
const locationH = new Vector3(50, 8, -130);
const locationI = new Vector3(70, 8, -100);
const locationJ = new Vector3(110, 8, -110);
const locationL = new Vector3(-40, 20, 0);
const locationM = new Vector3(-70, 20, 0);
const locationP = new Vector3(-40, 10, -85);

const infopanelsGroup = new Group();
const infoplaceholdersGroup = new Group();

infopanelsGroup.add(
    new InfoPanel(locationA, "assets/A-blok.png"),
    new InfoPanel(locationB, "assets/B-blok.png"),
    new InfoPanel(locationC, "assets/C-blok.png"),
    new InfoPanel(locationD, "assets/D-blok.png"),
    new InfoPanel(locationE, "assets/E-blok.png"),
    new InfoPanel(locationF, "assets/F-blok.png"),
    new InfoPanel(locationG, "assets/G-blok.png"),
    new InfoPanel(locationH, "assets/H-blok.png"),
    new InfoPanel(locationI, "assets/I-blok.png"),
    new InfoPanel(locationJ, "assets/J-blok.png"),
    new InfoPanel(locationL, "assets/L-blok.png"),
    new InfoPanel(locationM, "assets/M-blok.png"),
    new InfoPanel(locationP, "assets/P-blok.png")
)

infoplaceholdersGroup.add(
    new InfoPanelPlaceholder(locationA),
    new InfoPanelPlaceholder(locationB),
    new InfoPanelPlaceholder(locationC),
    new InfoPanelPlaceholder(locationD),
    new InfoPanelPlaceholder(locationE),
    new InfoPanelPlaceholder(locationF),
    new InfoPanelPlaceholder(locationG),
    new InfoPanelPlaceholder(locationH),
    new InfoPanelPlaceholder(locationI),
    new InfoPanelPlaceholder(locationJ),
    new InfoPanelPlaceholder(locationL),
    new InfoPanelPlaceholder(locationM),
    new InfoPanelPlaceholder(locationP)
)

export {infopanelsGroup, infoplaceholdersGroup};