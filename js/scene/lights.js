import {AmbientLight, PointLight} from "three";

const pointLight =  new PointLight(0xfffffff, 0.5, 0);
pointLight.position.set(0,200,0);

const ambientLight = new AmbientLight(0xaaaaaa, 1);

export {
    pointLight,
    ambientLight
}
