import '../styles/style.css';
import {Feature, Map, View} from 'ol';
import TileLayer from 'ol/layer/Tile';
import {TileWMS, XYZ} from "ol/source";
import {MVT} from "ol/format";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {Point} from "ol/geom";
import {fromLonLat} from "ol/proj";
import {Icon, Style} from "ol/style";

const arrowMarker = new Feature(new Point(fromLonLat([3.7085911, 51.0600373])));

const map = new Map({
  target: 'map',
  layers: [
    new TileLayer({
      // Other Tile source than OSM
      source: new XYZ({
        url: 'https://api.maptiler.com/tiles/satellite/{z}/{x}/{y}.jpg?key=YjqEfufnqNO4yvkZq253',
        format: new MVT(),
      }),
    }),
    new TileLayer({
      source: new TileWMS({
        url: 'https://geo.gent.be/geoserver/SG-E-Grenzen/wms',
        params: {
          'FORMAT': 'image/png',
          'VERSION': '1.1.1',
          tiled: true,
          "STYLES": '',
          "LAYERS": 'SG-E-Grenzen:Stadswijken',
          "exceptions": 'application/vnd.ogc.se_inimage',
          tilesOrigin: 94655.09380000085 + "," + 185681.89059999958
        }
      })
    }),
      //layer for odisee
    new VectorLayer({
      source: new VectorSource({
        features: [arrowMarker],
      }),
      style: new Style({
        image: new Icon({
          scale: 0.2,
          anchor: [0.5, 1],
          src: '../assets/arrow.png',
        }),
      }),
    })
  ],
  view: new View({
    center: [412826, 6631945],
    zoom: 17
  }),
  controls: [],
});

export {arrowMarker, map};


